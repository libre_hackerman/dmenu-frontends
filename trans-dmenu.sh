#!/bin/sh

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

declare -r TRANS_HISTORY="$HOME/.trans_history"

# Retrieve languages
languages="$(echo -e "es -> en\nen -> es\n\nes -> pt\npt -> es\n\nauto" | dmenu -c -i -l 7 -p "Lang >>")"
[ -n "$languages" ] || exit

# Create $TRANS_HISTORY if it doesn't exist
[ -f "$TRANS_HISTORY" ] || touch "$TRANS_HISTORY"

# Get previous translations from the same languages
hist="$(tac "$TRANS_HISTORY" | grep "^$languages" | xargs -d '\n' -I lang -- sh -c "echo 'lang' | cut -d '|' -f 2")"

# Retrieve sentence to translate
sentence="$(echo -e "$hist" | dmenu -c -i -l 15 -p "Translate >>")"
[ -n "$sentence" ] || exit

# Add languages and sentence to $TRANS_HISTORY if it doesn't start by space
if [ "$(echo "$sentence" | cut -c 1)" != " " ] && [ $(grep -c "^$languages|$sentence$" "$TRANS_HISTORY") -eq 0 ]; then
	echo "$languages|$sentence" >> "$TRANS_HISTORY"
fi

# Get trans option acording to language
if [ $(echo "$languages" | grep -c "^.. -> ..$") -eq 1 ]; then
	lang_op="$(echo "$languages" | sed "s/ -> /:/")"
else
	lang_op=""
fi

# Get translation
translation="$(trans -brief $lang_op "$sentence")"

# Display and copy translation
if [ -n "$translation" ]; then
	notify-send "Translation $languages" "$translation"
	echo -n "$translation" | xclip -selection clipboard
else
	notify-send -u critical "Translation $languages" "Couldn't perform translation"
fi
