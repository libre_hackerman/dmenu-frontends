#!/bin/sh

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

declare -r FORMAT="%artist% - %title%"

song="$(mpc listall -f "$FORMAT" | grep -v "^ - $" | sed "s/^ -/Unknown -/g" | sort | uniq | dmenu -c -i -l 15 -p "Song >>")"
[ -z "$song" ] && exit

# Try to find song in the playlist
i=1
IFS="
"
for song_playlist in $(mpc playlist -f "$FORMAT"); do
	if [ "$song_playlist" == "$song" ]; then
		mpc play $i
		exit
	fi
	i=$(($i+1))
done

# Get first file with matching $FORMAT
file_song="$(mpc listall -f "$FORMAT\t%file%" | grep "^$song	" | cut -f 2 | head -n 1)"
[ -z "$file_song" ] && exit 1

# Add song to playlist
mpc add "$file_song"
mpc play $i
