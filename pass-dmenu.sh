#!/bin/sh

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

cd ~/.password-store

entrys="$(find . -type f -name "*gpg" | xargs -d '\n' -I entry -- sh -c "echo 'entry' | cut -c 3- | rev | cut -c 5- | rev")"
sel="$(echo "$entrys" | dmenu -c -i -l 20 -p "pass >>")"

[ -n "$sel" ] || exit
pass -c "$sel" && notify-send "Password Store" "Copied $sel" || notify-send -u critical "Password Store" "Error opening $sel"
