#!/bin/sh

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

declare -r TITLE_MAX_LENGTH=60

# Check the database exists
if [ ! -d "$1" ]; then
	echo "Needs the path to Firefox profile"
	exit 1
fi

# Copy database to temporal file
db="$(mktemp /tmp/places.db.XXXXXXX)"
cp "$1/places.sqlite" "$db"

# Retrieve bookmarks titles
titles="$(echo "SELECT title FROM moz_bookmarks WHERE fk IS NOT NULL;" | sqlite3 "$db" | cut -c -$TITLE_MAX_LENGTH)"

# Ask which bookmark and double its quotes for sqlite
title="$(echo -e "$titles" | dmenu -c -i -l 15 -p "Bookmark >>" | sed 's/"/""/g')"
[ -z "$title" ] && rm "$db" && exit

# Get url
id="$(echo "SELECT fk FROM moz_bookmarks WHERE title LIKE \"$title%\" LIMIT 1;" | sqlite3 "$db")"
url="$(echo "SELECT url FROM moz_places WHERE id IS $id;" | sqlite3 "$db")"

# Open in Firefox
firefox "$url" &

# Delete temporary database copy
rm "$db"
