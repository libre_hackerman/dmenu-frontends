#!/usr/bin/env python3

# Copyright (C) 2019 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import subprocess
import os
from sys import stderr
from time import sleep
from datetime import datetime
from argparse import ArgumentParser

class UserExit(Exception):
    pass

class DmenuHandler:
    "This class calls Dmenu and processes user input."
    DELAY_OPTIONS = (0, 3, 5, 10, 20)

    # Name shown to the user: actual command
    PROGRAM_DICT = {"Kolourpaint": "kolourpaint",
                    "Ranger": "urxvtc -e ranger --selectfile",
                    "Sxiv": "sxiv"}

    def __init__(self, ask_selection, ask_delay, ask_program,
                 ask_include_mouse, ask_quality):
        self.ask_selection = ask_selection
        self.ask_delay = ask_delay
        self.ask_program = ask_program
        self.ask_include_mouse = ask_include_mouse
        self.ask_quality = ask_quality

    def show(self):
        """
        Prompt enabled questions.

        Return a tuple containing a string with the set of options for maim,
        the delay time and the program to open the screenshot.
        """
        options = []
        delay = 0
        program = None

        if self.ask_selection:
            options.append(self._prompt_selection())

        if self.ask_delay:
            delay = self._prompt_delay()

        if self.ask_program:
            program = self._prompt_program()

        if self.ask_include_mouse:
            options.append(self._prompt_include_mouse())

        if self.ask_quality:
            options.append(self._prompt_quality())

        return (" ".join(options), delay, program)

    def _dmenu(self, options, prompt):
        "Prompt dmenu with a set of options and return user selection."
        formated_options = "\n".join(map(str, options))
        proc_comp = subprocess.run(["dmenu", "-i", "-p", prompt],
                       encoding="UTF=8",
                       input=formated_options,
                       capture_output=True)

        if proc_comp.returncode == 1:
            raise UserExit()

        return proc_comp.stdout.rstrip()  # Deletes final \n

    def _prompt_selection(self):
        "Prompt dmenu asking selection options and return chosen option."
        if self._dmenu(("Fullscreen", "Rectangle"), "Area >>") == "Rectangle":
            return "-s"
        return ""

    def _prompt_delay(self):
        "Prompt dmenu asking delay time and return chosen option."
        return int(self._dmenu(DmenuHandler.DELAY_OPTIONS, "Delay >>"))

    def _prompt_program(self):
        "Prompt dmenu asking program to open the screenshot and return it."
        options = ["None"]
        options.extend(DmenuHandler.PROGRAM_DICT.keys())
        options.append("Other")

        program = self._dmenu(options, "Execute >>")

        if program == "None":
            return None
        elif program == "Other":
            return self._dmenu(self._read_dmenu_run_cache(), "Other >>")
        else:
            return DmenuHandler.PROGRAM_DICT.get(program)

    def _prompt_include_mouse(self):
        "Prompt dmenu asking if include mouse and return chosen option."
        if self._dmenu(("Show", "Hide"), "Cursor >>") == "Hide":
            return "-u"
        return ""

    def _prompt_quality(self):
        "Prompt dmenu asking quality level and return chosen option."
        return "-m " + self._dmenu(range(1, 11), "Compression quality >>")

    def _read_dmenu_run_cache(self):
        "Reads dmenu_run cache of programs and returns them in a list."
        try:
            with open(os.path.join(os.getenv("HOME"), '.cache/dmenu_run')) as f:
                programs = [line.rstrip() for line in f]

            return programs
        except:
            print("Error while reading dmenu_run cache", file=stderr)
            exit(1)


class MaimHandler:
    "This class takes screenshots using Maim."
    def __init__(self, filename, directory, image_format, delay, options,
                 show_notification, last_screenshot):
        self.filename = filename
        self.directory = directory
        self.image_format = image_format
        self.delay = delay
        self.options = options
        self.show_notification = show_notification
        self.last_screenshot = last_screenshot

    def take_screenshot(self):
        "Takes a screenshot and returns its path."
        maim = ["maim"]
        maim.extend(self.options.split())
        maim.extend(["-f", self.image_format])

        sleep(self.delay)
        proc_comp = subprocess.run(maim, capture_output=True)
        if proc_comp.returncode == 0:  # If maim ran with no error
            full_path, filename = self._write_image(proc_comp.stdout)
            if self.last_screenshot:
                link = self.directory + "/" + "last_screenshot." + \
                         self.image_format
                try:
                    os.remove(link)
                except FileNotFoundError:
                    pass
                os.symlink(full_path, link)

            if self.show_notification:
                self._show_success_notificacion(filename)

            return full_path
        else:
            print("Error while taking screenshot\n", file=stderr)
            print(proc_comp.stderr.decode("UTF-8"), file=stderr)

            if self.show_notification:
                self._show_error_notification("Error while taking screenshot")

            exit(1)

    def _write_image(self, image):
        "Writes an image to a file and returns a tuple (full path, filename)."
        filename = datetime.now().strftime(self.filename) + "." + self.image_format
        full_path = os.path.join(self.directory, filename)

        try:
            with open(full_path, "wb") as f:
                f.write(image)

            return (full_path, filename)
        except Exception as e:
            print("Error while writing screenshot to disk: " + str(e),
                  file=stderr)

            if self.show_notification:
                self._show_error_notification("Error while writing screenshot to disk")

            exit(1)

    def _show_success_notificacion(self, filename):
        "Shows a notification with the screenshot filename."
        subprocess.run(["notify-send", "-u", "low", "-a", "maim",
                       "Screenshot saved on " + filename])

    def _show_error_notification(self, message):
        "Shows a notification with an error message."
        subprocess.run(["notify-send", "-u", "critical", "-a", "maim",
                       "-t", "5000", message])


class Dmaim:
    def __init__(self):
        self.parser = ArgumentParser(prog="dmaim",
                                    description="Take a screenshot using maim"
                                     + " after asking for options with dmenu")
        self.parser.add_argument("-f", "--filename",
                                 default="screenshot-%H-%M-%S_%d-%m-%Y",
                                 help="Screenshot filename WITHOUT extension."
                                 + " Accepts date formatting (default: "
                                 + "screenshot-%%H-%%M-%%S_%%d-%%m-%%Y)")
        self.parser.add_argument("-d", "--directory",
                                default=os.getenv("HOME"),
                                help="Directory where to save the screenshot"
                                 + f" (default: {os.getenv('HOME')})")
        self.parser.add_argument("-F", "--format",
                                default="png", choices=["png", "jpg"],
                                help="Format in which to save the screenshot"
                                 + " (default: png)")
        self.parser.add_argument("-n", "--notifications", action="store_true",
                                help="Enable notifications. Needs libnotify")
        self.parser.add_argument("-o", "--default-options",
                                help="Default set of options to pass to maim")
        self.parser.add_argument("-l", "--last-screenshot",
                                 action="store_true", help="Creates a symbolic"
                                 + " link pointing to the screenshot taken")
        dmenu_questions = self.parser.add_argument_group("Dmenu questions",
                                "Flags to trigger Dmenu questions")
        dmenu_questions.add_argument("-s", "--ask-selection", action="store_true",
                                help="Prompt for fullscreen or selection")
        dmenu_questions.add_argument("-D", "--ask-delay", action="store_true",
                                 help="Prompt for delay in seconds")
        dmenu_questions.add_argument("-p", "--ask-program", action="store_true",
                                help="Prompt for external program to open the screenshot")
        dmenu_questions.add_argument("-m", "--ask-mouse", action="store_true",
                                help="Prompt about if include mouse cursor")
        dmenu_questions.add_argument("-q", "--ask-quality", action="store_true",
                                help="Prompt for compression quality (1 max, 10 min)")

    def start(self):
        "Parse arguments, opens Dmenu and takes screenshot."
        args = self.parser.parse_args()
        dmenu_handler = DmenuHandler(args.ask_selection, args.ask_delay,
                                    args.ask_program, args.ask_mouse,
                                    args.ask_quality)
        try:
            options, delay, program = dmenu_handler.show()
        except UserExit:
            exit(2)

        if args.default_options:  # If != None
            options += " " + args.default_options

        maim_handler = MaimHandler(args.filename, args.directory, args.format,
                                  delay, options, args.notifications,
                                  args.last_screenshot)

        scrnsht_path = maim_handler.take_screenshot()

        if program:
            program_splt = program.split()  # In case program has args
            program_splt.append(scrnsht_path)
            subprocess.run(program_splt)


if __name__ == "__main__":
    Dmaim().start()
