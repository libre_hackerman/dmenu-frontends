# Dmenu Frontends

This repository is a collection of scripts based on Dmenu that I've been writing for my personal needs.

## Warning

I'm using a [patched](https://tools.suckless.org/dmenu/patches/center) version of Dmenu that allows to center it on the screen. In these scripts, I'm using the option `-c` from that patch that will crash a normal Dmenu version. You can easily fix this by removing that option with `sed`. For example:
```
$ sed -i "s/dmenu -c/dmenu/g" trans-dmenu.sh
```
Note that the Python ones don't make use of the `-c` option and don't need to be modified.

## Translate Shell

![trans-dmenu](screenshots/trans-dmenu.gif)

This script allows to make translations from a set of hardcoded (but easily editable) languages plus auto-detection showing the translation in a desktop notification and copying it to the clipboard. It will also keep a separate history for each language of the translations requested (doesn't store the ones starting by space).

### Dependencies

- dmenu
- translate-shell
- xclip
- libnotify

##  Firefox Bookmarks

![firefox-bookmarks-dmenu](screenshots/firefox-bookmarks-dmenu.png)

This script allows to open a Firefox bookmark from Dmenu. It also allows to cut the lenght of the bookmark titles so the Dmenu window doesn't get too wide.
To launch it, you need to specify the path to your Firefox profile:
```
$ ./firefox-bookmarks-dmenu.sh $HOME/.mozilla/firefox/1234abcd.default-release
```

### Dependencies

- dmenu
- sqlite3
- firefox

## Mpc

![mpc-dmenu](screenshots/mpc-dmenu.png)

This script allows to choose a song from the MPD database and play it from the playlist (adding it if it's not there already).

### Dependencies

- dmenu
- mpc
- mpd

## Password Store

![pass-dmenu](screenshots/pass-dmenu.png)

This script allows to select an entry from your `pass` and copy it to the clipboard.

### Dependencies

- dmenu
- pass
- libnotify

## DMOpener

![dmopener](screenshots/dmopener.png)

DMOpener is a simple Dmenu based file explorer with the only purpose of opening them. By default it sorts files by date.

### Implementations

The program is available in both Python and Shell Script.

### Launcher

By default it opens files with [slauncher](https://gitlab.com/libre_hackerman/slauncher), but you can replace the `LAUNCHER` constant in the scripts with a different one, like **xdg-open**.

### Dependencies

- dmenu
- python3 (optional)
- slauncher (optional)

## Maim

![dmaim](screenshots/dmaim.gif)

This Python script allows to take screenshots selecting between several options.

You can edit delays suggestions and programs list modifying `DELAY_OPTIONS` and `PROGRAM_DICT` at the beggining of DmenuHandler class.

```
usage: dmaim [-h] [-f FILENAME] [-d DIRECTORY] [-F {png,jpg}] [-n]
             [-o DEFAULT_OPTIONS] [-s] [-D] [-p] [-m] [-q]

Take a screenshot using maim after asking for options with dmenu

optional arguments:
  -h, --help            show this help message and exit
  -f FILENAME, --filename FILENAME
                        Screenshot filename WITHOUT extension. Accepts date
                        formatting (default: screenshot-%H-%M-%S_%d-%m-%Y)
  -d DIRECTORY, --directory DIRECTORY
                        Directory where to save the screenshot (default:
                        /home/lordpu239)
  -F {png,jpg}, --format {png,jpg}
                        Format in which to save the screenshot (default: png)
  -n, --notifications   Enable notifications. Needs libnotify
  -o DEFAULT_OPTIONS, --default-options DEFAULT_OPTIONS
                        Default set of options to pass to maim
  -l, --last-screenshot
                        Creates a symbolic link pointing to the screenshot taken

Dmenu questions:
  Flags to trigger Dmenu questions

  -s, --ask-selection   Prompt for fullscreen or selection
  -D, --ask-delay       Prompt for delay in seconds
  -p, --ask-program     Prompt for external program to open the screenshot
  -m, --ask-mouse       Prompt about if include mouse cursor
  -q, --ask-quality     Prompt for compression quality (1 max, 10 min)
  ```

### Dependencies

- dmenu
- python3
- maim
- libnotify (optional)
