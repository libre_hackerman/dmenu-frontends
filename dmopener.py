#!/usr/bin/env python

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from sys import argv, stderr
from os import getenv, listdir, chdir, getcwd
from os.path import isdir, getmtime, basename
from subprocess import run

ITEMS_LISTED = 15
LAUNCHER = "slauncher"

def dmopener(open_this):
    if isdir(open_this):
        chdir(open_this)

        dir_content = sorted(listdir(), key=getmtime, reverse=True)
        # Remove hidden files
        dir_content = [f for f in dir_content if not f.startswith(".")]
        # Add .. option
        dir_content.insert(ITEMS_LISTED - 1, "..")

        proc_comp = run(["dmenu", "-i", "-l", str(ITEMS_LISTED),  "-p",
                         "{} >>".format(basename(getcwd()))],
                        encoding="UTF-8", capture_output=True,
                        input="\n".join(dir_content))
        if proc_comp.returncode != 1:
            dmopener("{}/{}".format(open_this, proc_comp.stdout.rstrip()))
    else:
        run([LAUNCHER, open_this])


if __name__ == "__main__":
    if len(argv) == 1:
        starting_dir = getenv("HOME")
    elif len(argv) == 2:
        starting_dir = argv[1]
    else:
        print("Usage:", argv[0], "[DIR/FILE]", file=stderr)
        exit(1)

    dmopener(starting_dir)
