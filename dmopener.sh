#!/bin/sh

# Copyright (C) 2020 Esteban López Rodríguez <gnu_stallman@protonmail.ch>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

declare -r ITEMS_LISTED=15
declare -r LAUNCHER="slauncher"

IFS="
"

function dmopener()
{
	if [ -d "$1" ]; then
		cd "$1"

		# Create string with directory items and ".." in the last shown
		# position in Dmenu
		local dir_content=""
		local i=0 dots_added=0
		for f in $(ls -t); do
			if [ $i -eq $(($ITEMS_LISTED-1)) ]; then
				dir_content+="..\n"
				dots_added=1
			fi

			dir_content+="$f\n"
			i=$(($i+1))
		done
		[ $dots_added -eq 0 ] && dir_content+="..\n"
		
		local sel="$(echo -en "$dir_content" | dmenu -c -i -l $ITEMS_LISTED \
			-p "$(basename $PWD) >>")"
		[ -n "$sel" ] && dmopener "$sel"
	else
		$LAUNCHER "$1"
	fi
}


if [ $# -eq 0 ]; then
	starting_dir=$HOME
elif [ $# -eq 1 ]; then
	starting_dir=$1
else
	echo "Usage: $0 [DIR/FILE]" >> /dev/stderr
	exit 1
fi

dmopener "$starting_dir"
